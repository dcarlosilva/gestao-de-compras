<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssociacoesLotes extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'associacoes_lotes';
    protected $fillable = [
        'nome',
        'filial',
        'projeto',
        'numero_patrimonio',
        'requisicao',
        'tipo',
        'data_entrega',
        'id_lote',
        'id_usuario_associacao'
    ];
    public function lote()
    {
        // Esta associção pertence a um lote em especifico
        return $this->belongsTo(Lotes::class,'id_lote');
    }
    
    public function subistituicao()
    {
        return $this->hasOne(Subistituicoes::class,'id_associcao','id');
    }
    
    public function usuario()
    {
        // Este lote pode ter Varias Associações
        return $this->hasOne(User::class,'id','id_usuario_associacao');
    }
}
