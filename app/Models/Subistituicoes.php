<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subistituicoes extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'subistiuicao';
    protected $fillable = [
        'numero_patrimonio',
        'motivo',
        'tipo_subistituicao',
        'id_associcao',
        'id_usuario_substituicao'
    ];

    public function usuario()
    {
        // Este lote pode ter Varias Associações
        return $this->hasOne(User::class,'id','id_usuario_substituicao');
    }
}
