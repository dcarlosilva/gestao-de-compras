<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lotes extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'lotes';
    protected $fillable = [
        'vendedor',
        'modelo',
        'tipo',
        'quantidade',
        'quantidade_disponivel',
        'data_compra',
        'valor',
        'id_usuario',
        'nfe',
        'link_arquivo_nfe',
        'requisicao'
    ];
    public function associacao()
    {
        // Este lote pode ter Varias Associações
        return $this->hasMany(AssociacoesLotes::class,'id_lote');
    }
    public function usuario()
    {
        // Este lote pode ter Varias Associações
        return $this->hasOne(User::class,'id','id_usuario');
    }
   
}
