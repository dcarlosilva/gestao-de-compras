<?php

namespace App\Http\Controllers;

use App\Models\AssociacoesLotes as ModelsAssociacoesLotes;
use App\Models\Lotes;
use App\Http\Requests\AssociacaoFormRequest;
use Illuminate\Http\Request;
use App\Service\GerenciadorDeEstoque;
use App\Service\RemovedorDeDados;

class AssociacaoController extends Controller
{

    public function associacaoLista(int $idLote, Request $request, GerenciadorDeEstoque $estoque)
    {
        // Lista os colabores associado a lote

        $dados = Lotes::find($idLote)->associacao;


        $request->session()->put('id_lote', $idLote);
        $msg = $request->session()->get('msg');

        return view('associacao.listar', [
            'dados' => $dados, 'msg' => $msg,
            'liberar_cadastro' => $estoque->validarDisponibilidade($idLote)
        ]);
    }

    public function exibirFormularioDeCadastro(Request $request)
    {
        // Caregar id do lote para salvar no Formulario
        $idLote = $request->session()->get('id_lote');


        return view('associacao.incluir', ['id_lote' => $idLote]);
    }

    public function store(AssociacaoFormRequest $request, GerenciadorDeEstoque $estoque)
    {

        $dados = $request->except('_token');
        $idLote = $dados['id_lote'];
        // Salva colaborado alocados na tabela das associacao
        ModelsAssociacoesLotes::create($dados);

        $estoque->atualizarQuantidade($idLote, 'adiciona');

        $request->session()
            ->flash('msg', "Associação Incluida com sucesso: " . $dados['nome']);

        return redirect('/lotes/associacao/lista/' . $idLote);
    }
    public function destroy(Request $request, GerenciadorDeEstoque $estoque, RemovedorDeDados $remover)
    {
        $id_lote = $request->session()->get('id_lote');
        $request->session()
            ->flash('msg', "Associação Excluida com sucesso: $request->id");

        $remover->associacao($request->id);

        $estoque->atualizarQuantidade($id_lote, 'destroy');

        return redirect('/lotes/associacao/lista/' . $id_lote);
    }
}
