<?php

namespace App\Http\Controllers;

use App\Http\Requests\LotesFormRequest;
use App\Http\Requests\SubstituicaoFormRequest;
use App\Models\Subistituicoes;
use App\Service\GerenciadorDeAssociacao;
use App\Service\GerenciadorDeLote;
use Illuminate\Http\Request;

class SubistituicaoController extends Controller
{
  public function index(GerenciadorDeAssociacao $associacao)
  {
    $dados = $associacao->subistituicaoGet();
    return view('substituicao.listar', ['dados' => $dados]);
  }
  public function store(SubstituicaoFormRequest $request)
  {
    $dados = $request->except('_token');
    Subistituicoes::create($dados);
    return redirect('/subistituicao');
  }
}
