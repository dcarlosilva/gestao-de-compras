<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;

class EntrarController extends Controller
{
    public function index()
    {
       
        return view('entrar.login');
    }
    public function entrar(Request $request)
    {
        if (!Auth::attempt($request->only(['email', 'password']))) {

            return back()
                ->withErrors('Usuário e/ou senha errado ');
        };
        return redirect ('dashboard');
    }
}
