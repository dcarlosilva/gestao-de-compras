<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lotes;


class DashboardController extends Controller
{
    public function index()
    {

        $dados = Lotes::all();
        $quantidadeNotebook = $dados->where('tipo', 'Notebook')
            ->sum('quantidade');
        $quantidadeNotebookDisponivel = $dados->where('tipo', 'Notebook')
            ->sum('quantidade_disponivel');
        $valorTotalDeComprasNotebook = $dados->where('tipo', 'Notebook')
            ->sum('valor');
        $quantidadeCelular = $dados->where('tipo', 'Celular')
            ->sum('quantidade');
        $quantidadeCelularDisponivel = $dados->where('tipo', 'Celular')
            ->sum('quantidade_disponivel');
        $valorTotalDeComprasCelular = $dados->where('tipo', 'Celular')
            ->sum('valor');
        return view('dashboard.dashboard', [
            'quantidadeNotebook' => $quantidadeNotebook,
            'quantidadeNotebookDisponivel' => $quantidadeNotebookDisponivel,
            'valorTotalDeComprasNotebook' =>  $valorTotalDeComprasNotebook,
            'quantidadeCelular' => $quantidadeCelular,
            'quantidadeCelularDisponivel' => $quantidadeCelularDisponivel,
            'valorTotalDeComprasCelular' => $valorTotalDeComprasCelular,
        ]);
    }
}
