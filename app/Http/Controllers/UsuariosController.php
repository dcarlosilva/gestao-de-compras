<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsuariosController extends Controller
{

    public function index()
    {
        $dados = User::all();
        return view('usuario.listar', ['dados' => $dados]);
    }
    public function exibirFomulario()
    {
        return view('usuario.registrar');
    }
    public function store(Request $request)
    {
        $date = $request->except('_token');
        $date['password'] = Hash::make($date['password']);
        User::create($date);
        return redirect('usuarios');
    }
    public function editar($id)
    {
        $dados = User::find($id);

        return view('usuario.editar', ['dados' => $dados]);
    }
    public function update($id, Request $request)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request['password']);
        $user->save();
        return redirect('usuarios');
    }

    public function delete($id)
    {

        $user = User::find($id);
        $user->delete();
        return back();
    }
}
