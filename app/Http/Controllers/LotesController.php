<?php

namespace App\Http\Controllers;

use App\Http\Requests\LotesFormRequest;
use App\Models\Lotes;
use App\Service\GerenciadorDeLote;
use App\Service\RemovedorDeDados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Service\GerenciadorDeArquivos;

class LotesController extends Controller
{


    public function index(Request $request)
    {
        // Retorna a lista com os lotes para view da tabelas lotes.blade
        $dados = Lotes::all();

        $msg = $request->session()->get('msg');
        $erro = $request->session()->get('erro');

        return view('lotes.listar', [
            'dados' => $dados,
            'msg' => $msg,
            'erro' => $erro,

        ]);
    }

    public function criarLote()
    {

        return view('lotes.novo');
    }

    public function editaLote(Lotes $lotes, $id)
    {

        return view('lotes.editar', [
            'dados' => $lotes->find($id)
        ]);
    }
    public function update(GerenciadorDeLote $gerenciadorDeLote, LotesFormRequest $request)
    {

        $gerenciadorDeLote->atualizarDados($request);
        return redirect('/lotes');
    }

    public function store(LotesFormRequest $request, GerenciadorDeArquivos $gerenciadorDeArquivos)
    {

        // Request sendo validada na class LotesFormRequest onde é recuperado os dados

        $dadosRequest = $request->except('_token');
        $quantidade = ['quantidade_disponivel' => $dadosRequest["quantidade"]];
        $dados = array_merge($dadosRequest, $quantidade);

        //    Tratando erro no banco protegendo a gravação dos registros do lote
        // try {
        //     DB::beginTransaction();
        // $lotes = Lotes::create($dados);
        //     DB::commit();
        // } catch (\Throwable $th) {
        //     DB::rollBack();
        // }
        $lotes = Lotes::create($dados);
        // Enviado arquivo do Formulário
        $link = $gerenciadorDeArquivos->enviarNfeLote(
            $lotes->id,
            $request->input('nfe'),
            $request->file('nfeFile')->getClientOriginalExtension(),
            $request->file('nfeFile')->get()
        );
        // Salvando URL da Nfe no banco de dados.
        $lote = Lotes::find($lotes->id);
        $lote->link_arquivo_nfe    = $link;
        $lote->save();

        $request->session()
            ->flash('msg', "Lote Cadastrado com sucesso: {$lote->id}");
        return redirect('/lotes');
    }

    public function destroy(Request $request, int $id, RemovedorDeDados $remover)
    {
        // Recebe o ID da Request via URL, manda para objeto RemovedorDeLote no metodo
        // remover->lote que retorna um True ou False envia para session para liberar a DIV 
        // na viewer da rota /lotes 

        $remover->lote($id) ? $request->session()
            ->flash('msg', "Lote $id Excluído com sucesso") :
            $request->session()
            ->flash('erro', "Erro ao Excluído o lote: $id");

        return redirect('/lotes');
    }
}
