<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LotesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vendedor' => 'required|max:100',
            'nfe' => 'required|numeric',
            'modelo' => 'required|max:100',
            'quantidade' => 'required|numeric',
            'requisicao' => 'required|numeric',
            'valor' => 'required|numeric',
            'data_compra'=>'required',
            'nfeFile' => 'mimes:png,jpg,jpeg,pdf|max:2048'
            
        ];
    }
    public function messages()
    {
        return [
            'required' => 'O campo :attribute é obrigatório',
            'numeric' => 'O campo :attribute tem que ser um numero',
            'max'=> 'Valor máximo do campo :attribute exigido ',
            
        ];
    }
}
