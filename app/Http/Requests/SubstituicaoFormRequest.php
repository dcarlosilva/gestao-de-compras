<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubstituicaoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'numero_patrimonio' => 'required|numeric',
            'motivo' => 'required|max:100'
        ];
    }
    public function messages()
    {
        return [
            'required' => 'O campo :attribute é obrigatorio',
            'numeric' => 'O campo :attribute tem que ser um numero',
            'max' => 'Valor maixmo do campo :attribute exedido '
        ];
    }
}
