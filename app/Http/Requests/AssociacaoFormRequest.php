<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssociacaoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nome"=>"required|max:100",
            "filial"=>"required|max:100",
            "projeto"=>"required|max:100",
            "numero_patrimonio"=>"required|numeric",
            "data_entrega"=>"required|max:100",
            "requisicao"=>"required|numeric"

        ];
    }
    public function messages()
    {
        return [
            'required' => 'O campo :attribute é obrigatorio',
            'numeric' => 'O campo :attribute tem que ser um numero',
            'max'=> 'Valor maixmo do campo :attribute exedido '
        ];
    }
}
