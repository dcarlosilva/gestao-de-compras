<?php

namespace App\Service;

use App\Models\AssociacoesLotes;
use App\Models\Lotes;
use Illuminate\Support\Facades\DB;

class RemovedorDeDados
{
    public function lote(int $idLote)
    {
        try {
            DB::beginTransaction();
            $lotes = Lotes::find($idLote);

            // Exluindo Associacoes dos laotes
            $lotes->associacao->each(function (AssociacoesLotes $associao) {
                $associao->delete();
            });

            $resutado  = $lotes->delete();
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            $resutado = false;
        }

        return $resutado;
    }
    public function associacao($id)
    {
        try {
            DB::beginTransaction();
            $associacao = AssociacoesLotes::find($id);
            $resutado  = $associacao->delete();
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            $resutado = false;
        }

        return $resutado;
    }
}
