<?php

namespace App\Service;

use App\Models\Lotes;
use PhpParser\Node\Expr\New_;

class GerenciadorDeLote
{
    public function atualizarDados($dados)
    {

        $lotes = Lotes::find($dados->id_lote);

        $lotes->vendedor = $dados->vendedor;
        $lotes->nfe = $dados->nfe;
        $lotes->tipo = $dados->tipo;
        $lotes->modelo = $dados->modelo;
        $lotes->quantidade = $dados->quantidade;
        $lotes->requisicao = $dados->requisicao;
        $lotes->data_compra = $dados->data_compra;
        $lotes->valor = $dados->valor;

        $lotes->quantidade_disponivel = $this->atualizarQuantidade(
            $dados->id_lote,
            $dados->quantidade
        );

        $lotes->save();
    }

    private function atualizarQuantidade($id, int $quantidadeNova): int
    {
        $lotes = new Lotes();
        $estoque = $lotes->find($id);


        if ($quantidadeNova != intval($estoque->quantidade)) {

            $quantidadeDiferencia =  $quantidadeNova - intval($estoque->quantidade);
            $valorTotal =   intval($estoque->quantidade_disponivel) + $quantidadeDiferencia;


            if ($valorTotal <= 0) {

                dd('Enviar erro para a viwer valor menor que já utilizado do estoque');
            }
            return $valorTotal;
        }
        return intval($estoque->quantidade);
    }
}
