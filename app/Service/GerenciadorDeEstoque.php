<?php

namespace App\Service;

use App\Models\Lotes;
use GuzzleHttp\Psr7\Request;
use App\Models\AssociacoesLotes as Associacao;
use Illuminate\Database\Eloquent\Model;

/**
 *Mudar nome da classe para uma ação GerenciadorDeEstoque

*/
class GerenciadorDeEstoque
{
    public function validarDisponibilidade($idLote)
    {
        $count_utilizado = Associacao::where('id_lote', $idLote)
            ->count();
        $quantidade_disponivel = Lotes::where('id', $idLote)
            ->get('quantidade_disponivel')
            ->toArray();
        $totalDisponivel = (int)$quantidade_disponivel[0]['quantidade_disponivel'];

        if ($totalDisponivel > 0) {
            $disponivel = true;
        } else {
            $disponivel = false;
        }
        return $disponivel;
    }
    public function atualizarQuantidade($idLote, $tipo)
    {
        $quantidade_disponivel = Lotes::where('id', $idLote)
            ->get('quantidade_disponivel')
            ->toArray();
        $totalDisponivel = (int)$quantidade_disponivel[0]['quantidade_disponivel'];
        if ($tipo == 'adiciona') {
            $atualizar = $totalDisponivel - 1;
        } else {
            $atualizar = $totalDisponivel + 1;
        }
        Lotes::where('id', $idLote)->update(['quantidade_disponivel' => $atualizar]);
    }
}
