<?php

namespace App\Service;

use Illuminate\Support\Facades\Storage;


class GerenciadorDeArquivos
{

    private $basePath = 'order-manager/';


    private function getCaminhoArquivoNfeLote($NomeDiretorioDoLote, $nomeArquivo)
    {
        return $this->basePath . 'lotes/' . $NomeDiretorioDoLote . '/' . $nomeArquivo;
    }
    public function enviarNfeLote($idLote = "teste/", $numeroDaNfe, $extensaoDoArquivo, $conteudo)
    {
        $nomeArquivo = $numeroDaNfe . '.' . $extensaoDoArquivo;

        Storage::put($this->getCaminhoArquivoNfeLote($idLote, $nomeArquivo), $conteudo,'public');
        
        $url = Storage::url(
            $this->getCaminhoArquivoNfeLote($idLote, $nomeArquivo)

        );
        return $url;
    }
    
}
