<?php

use App\Http\Controllers\AssociacaoController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\LotesController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\EntrarController;
use App\Http\Controllers\SubistituicaoController;
use App\Http\Controllers\UsuariosController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [EntrarController::class, 'index'])->name('index');

Route::post('/', [EntrarController::class, 'entrar'])->name('login');


// Lotes visualizar
Route::get('/lotes', [LotesController::class, 'index'])->name('lotes')->middleware('auth');
Route::get('/lotes/criar', [LotesController::class, 'criarLote'])->name('criar_lote')->middleware('auth');
Route::get('/lotes/editar/{id_lote}', [LotesController::class, 'editaLote'])->name('editar')->middleware('auth');

// Atualizar lote

Route::post('/lotes/editar/{id_lote}', [LotesController::class, 'update'])->middleware('auth');


// Dashboard
Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard')->middleware('auth');
// Gravar novo lote
Route::post('/lotes/criar', [LotesController::class, 'store'])->middleware('auth');

// Associações de lotes
Route::get('/lotes/associacao/lista/{id}', [AssociacaoController::class, 'associacaoLista'])->middleware('auth');
Route::get('/lotes/associacao/incluir', [AssociacaoController::class, 'exibirFormularioDeCadastro'])->name('incluir_novo')->middleware('auth');


Route::post('/lotes/associacao/formulario_salvar', [AssociacaoController::class, 'store'])->middleware('auth');
Route::post('/lotes/excluir/{id}', [LotesController::class, 'destroy']);
Route::post('/lotes/excluir/associacao/{id}', [AssociacaoController::class, 'destroy'])->middleware('auth');

// subistituições
Route::get('/subistituicao', [SubistituicaoController::class, 'index'])->name('subistituicao')->middleware('auth');
Route::post('/subistituicao', [SubistituicaoController::class, 'store'])->middleware('auth');

// Usuarios

Route::get('/usuarios', [UsuariosController::class, 'index'])->name('usuarios')->middleware('auth');
Route::get('/usuarios/criar', [UsuariosController::class, 'exibirFomulario'])->name('usuarios.criar');
Route::post('/usuarios/criar', [UsuariosController::class, 'store']);
Route::get('/usuarios/editar/{id}', [UsuariosController::class, 'editar'])->name('usuarios.editar')->middleware('auth');
Route::post('/usuarios/editar/{id}', [UsuariosController::class, 'update'])->name('usuarios.editar')->middleware('auth');
Route::post('/usuarios/excluir/{id}', [UsuariosController::class, 'delete'])->name('usuarios.excluir')->middleware('auth');
Route::get('/logout', function () {Auth::logout(); return redirect('/');})->name('logout');