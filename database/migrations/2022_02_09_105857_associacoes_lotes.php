<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AssociacoesLotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associacoes_lotes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->integer('id_lote')->unsigned();
            $table->foreign('id_lote')->references('id')->on('lotes');
            $table->bigInteger('id_usuario_associacao')->unsigned();
            $table->foreign('id_usuario_associacao')->references('id')->on('users');
            $table->string('filial');
            $table->string('projeto');
            $table->integer('numero_patrimonio');
            $table->integer('requisicao');
            $table->string('tipo');
            $table->string('data_entrega');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associacoes_lotes');
    }
}
