<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lotes', function (Blueprint $table) {

            $table->increments('id');

            $table->string('vendedor');
            $table->string('modelo');
            $table->integer('quantidade');
            $table->integer('requisicao');
            $table->integer('quantidade_disponivel');
            $table->integer('id_usuario')->references('id')->on('users');
            $table->string('tipo');
            $table->string('data_compra');
            $table->double('valor');
            $table->integer('nfe');
            $table->string('link_arquivo_nfe')->default('');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lotes');
    }
}
