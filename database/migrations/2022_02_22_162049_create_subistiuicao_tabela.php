<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubistiuicaoTabela extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subistiuicao', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_associcao')->unsigned();
            $table->foreign('id_associcao')->references('id')->on('associacoes_lotes');
            
            $table->integer('id_usuario_substituicao')->references('id')->on('users');
            $table->integer('numero_patrimonio');
            $table->string('tipo_subistituicao');
            $table->string('motivo');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subistiuicao');
    }
}

