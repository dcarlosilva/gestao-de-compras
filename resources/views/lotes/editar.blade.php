@extends('layout.index')
<!DOCTYPE html>
@section('conteudo')
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
    </head>

    <body>
        <div class="container-md">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="POST">
                @csrf
                <div class="form-group">
                    <label for="formGroupExampleInput">Vendedor</label>
                    <input type="text" name="vendedor" class="form-control" id="vendedor" placeholder="" value="{{$dados->vendedor}}">
                </div>
                <div class="form-group">
                    <label for="nfe">Nfe</label>
                    <input type="text" name="nfe" class="form-control" id="nfe" placeholder="" value="{{$dados->nfe}}" >
                </div>
                <div class="form-group">
                    <label for="tipo">Tipo</label>
                    <select class="form-control" id="tipo" name="tipo">
                        <option value="Notebook">Notebook</option>
                        <option value="Celular">Celular</option>
                        <option value="Tablet">Tablet</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="modelo">Modelo</label>
                    <input type="text" name="modelo" class="form-control" id="modelo" placeholder="" value="{{$dados->modelo}}">
                </div>
                <div class="form-group">
                    <label for="quantidade">Quantidade</label>
                    <input type="text" name="quantidade" class="form-control" id="quantidade" placeholder="" value="{{$dados->quantidade}}">
                </div>
                <div class="form-group">
                    <label for="requisicao">Numero da requisição de compra</label>
                    <input type="text" name="requisicao" class="form-control" id="requisicao" placeholder="" value="{{$dados->requisicao}}">
                </div>
                <div class="form-group ">

                    <div class="row align-items-start">
                        <div class="col-md-auto">
                            <label for="data_compra">Data da Compra</label>
                            <input type="text" name="data_compra" class="form-control" id="data_compra" placeholder="" value="{{$dados->data_compra}}">
                        </div>
                        <div class="col-md-auto">
                            <label for="valor">Valor</label>
                            <input type="text" name="valor" class="form-control" id="valor" placeholder="" value="{{$dados->valor}}">
                        </div>
                        <div class="col-md-auto">
                            <label for="exampleFormControlFile1">Anexar arquivo da Nfe:</label>
                            <input type="file" class="form-control-file" id="exampleFormControlFile1">
                        </div>

                    </div>
                </div>
                <div class=" row form-group">
                    <div class="col align-items-end">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </div>

            </form>
        </div>
    </body>

    </html>


@endsection
