<!DOCTYPE html>
@extends('layout.index')
@section('conteudo')
    <html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>SB Admin 2 - Tables</title>

        <!-- Custom fonts for this template -->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">

        <!-- Custom styles for this page -->
        <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

    </head>



    <body id="page-top">


        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <h1 class="h3 mb-2 text-gray-800">Lotes</h1>
            <br>Incluir Nfs de compra por grupo<br>


            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <a href="{{ route('criar_lote') }}" title="Editar Lote">
                        <button type="button" class="btn btn-primary">Novo Lote</button>
                    </a>
                </div>
                @if (!empty($msg))
                    <div class="alert alert-success" role="alert">
                        {{ $msg }}
                    </div>
                @endif
                @if (!empty($erro))
                    <div class="alert alert-danger" role="alert">
                        {{ $erro }}
                    </div>
                @endif
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Vendedor</th>
                                    <th>Nfe</th>
                                    <th>Modelo</th>
                                    <th>Tipo</th>
                                    <th>Quantidade</th>
                                    <th>Quantidade Disponivel</th>
                                    <th>Data da compra</th>
                                    <th>Valor</th>
                                    <th>Açoes</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Vendedor</th>
                                    <th>Nfe</th>
                                    <th>Modelo</th>
                                    <th>Tipo</th>
                                    <th>Quantidade</th>
                                    <th>Quantidade Disponivel</th>
                                    <th>Data da compra</th>
                                    <th>Valor</th>
                                    <th>Açoes</th>
                                </tr>
                            </tfoot>
                            @foreach ($dados as $dados)
                                <tr>
                                    <td>
                                        {{ $dados->id }}
                                    </td>
                                    <td>
                                        {{ $dados->vendedor }}
                                    </td>
                                    <td><a href={{ $dados->link_arquivo_nfe }} title="Baixar arquivo da Nfe"
                                            target="_blank">{{ $dados->nfe }}</a>
                                    </td>
                                    <td>{{ $dados->modelo }}</td>
                                    <td>{{ $dados->tipo }}</td>
                                    <td>{{ $dados->quantidade }}</td>
                                    <td>{{ $dados->quantidade_disponivel }}</td>
                                    <td>{{ $dados->data_compra }}</td>
                                    <td>
                                        <div id={{ $dados->id }}>R$</div>
                                        <script>
                                            formatReal(
                                                getMoney({{ $dados->valor }}), {{ $dados->id }})
                                        </script>
                    </div>
                    </td>
                    <td>

                        <!-- Botão para acionar detalhes -->
                        <div class="container-fluid">
                            <div class="row">

                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-link" data-toggle="modal"
                                        data-target="#a{{ $dados->id }}">
                                        <i class="fa-solid fa-magnifying-glass"></i>
                                    </button>
                                </div>

                                <!-- Modal detalhes -->
                                <div class="modal fade" id="a{{ $dados->id }}" tabindex="-1" role="dialog"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="{{ $dados->id }}">
                                                    Detalhes do
                                                    Lote {{ $dados->id }}</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Fechar">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">

                                                <p><b>Nfe: </b> {{ $dados->nfe }}</p>
                                                <p><b>Vendedor: </b> {{ $dados->vendedor }}</p>
                                                <p><b>Valor da compra: </b>
                                                <div id="b{{ $dados->id }}">R$</div>
                                                <script>
                                                    formatReal(
                                                        getMoney({{ $dados->valor }}), "b{{ $dados->id }}")
                                                </script>
                                                </p>
                                                <p><b>Quantidade comprado: </b>{{ $dados->quantidade }}
                                                </p>
                                                <p><b>Quantidade disponivel:
                                                    </b>{{ $dados->quantidade_disponivel }}</p>
                                                <p><b>Quantidade de associações:
                                                    </b>{{ $dados->associacao->count() }}</p>
                                                <p><b>Numero da solicitação:</b> {{ $dados->requisicao }}
                                                </p>
                                                <p><b>Data da Compra:</b> {{ $dados->data_compra }}</p>
                                                <p><b>Criado em: </b>{{ $dados->created_at }}</p>
                                                <p><b>Criado por: </b>{{ $dados->usuario->name }}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Fechar</button>
                                                <button type="subimit" class="btn btn-info "
                                                    onClick="window.print()">Imprimir</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <form name='excluir' action="/lotes/excluir/{{ $dados->id }}" method="post">
                                    @csrf
                                    <div class="col-sm-4">
                                        <!-- Botão para acionar excluir -->
                                        <button type="button" class="btn btn-link" data-toggle="modal"
                                            data-target="#p{{ $dados->id }}">
                                            <i class='fas fa-trash-alt'></i>
                                        </button>
                                    </div>
                                    <!-- Modal Excluir -->
                                    <div class="modal fade" id="p{{ $dados->id }}" tabindex="-1" role="dialog"
                                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="{{ $dados->id }}">
                                                        Deseja
                                                        excluir
                                                        o lote?</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Fechar">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p><b>Nfe: </b> {{ $dados->nfe }}</p>
                                                    <p><b>Você selecionou o lote: </b> {{ $dados->id }}
                                                    </p>
                                                    <p><b>Vendedor: </b> {{ $dados->vendedor }}</p>
                                                    <p><b>Quantidade: </b>{{ $dados->quantidade }}</p>
                                                    <p><b>Quantidade Disponivel:
                                                        </b>{{ $dados->quantidade_disponivel }}</p>
                                                    <p><b>Quantidade de associações no lote:
                                                        </b>{{ $dados->associacao->count() }}</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Fechar</button>
                                                    <button type="subimit" class="btn btn-danger ">EXCLUIR</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            {{-- Links --}}
                            <div class="row">
                                <div class="col-sm-4">
                                    <a href="{{ route('editar', $dados->id) }}" title="Editar Lote ">
                                        <button type="button" class="btn btn-link">
                                            <i class='far fa-edit'></i>
                                        </button>
                                    </a>
                                </div>
                                <div class="col-sm-4">
                                    <a href="/lotes/associacao/lista/{{ $dados->id }}" title="Visualizar Associações">
                                        <button type="button" class="btn btn-link">
                                            <i class='fas fa-folder-open'></i>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </td>
                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>

        </div>
        <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; Your Website 2020</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->


    </body>

    </html>
@endsection
