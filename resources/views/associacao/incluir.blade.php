@extends('layout.index')
@section('conteudo')


    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title></title>
        <script src="https://kit.fontawesome.com/b8a1f4a2d8.js" crossorigin="anonymous"></script>
    </head>

    <body>
        <div class="container-md">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="POST" action="/lotes/associacao/formulario_salvar">
                @csrf
                <INPUT TYPE="hidden" name="id_lote" value={{ $id_lote }}>
                <INPUT TYPE="hidden" name="id_usuario_associacao" value={{ Auth::user()->id }}>
                <div class="form-group">
                    <label for="Nome">Nome</label>
                    <input type="text" name="nome" class="form-control" id="nome" placeholder="">
                </div>
                <div class="form-group">
                    <label for="filial">Filial</label>
                    <input type="text" name="filial" class="form-control" id="filial" placeholder="">
                </div>
                <div class="form-group">
                    <label for="tipo">Tipo</label>
                    <select class="form-control" id="tipo" name="tipo">
                        <option value="Aumento de quadro">Aumento de quadro</option>
                        <option value="Substituição">Substituição</option>

                    </select>
                </div>
                <div class="form-group">
                    <label for="Projeto">Projeto</label>
                    <input type="text" name="projeto" class="form-control" id="projeto" placeholder="">
                </div>
                <div class="form-group">
                    <label for="numero_patrimonio">Nmero do partimonio</label>
                    <input type="text" name="numero_patrimonio" class="form-control" id="numero_patrimonio"
                        placeholder="">
                </div>
                <div class="form-group">
                    <label for="data_entrega">Data da Entrega</label>
                    <input type="text" name="data_entrega" class="form-control" id="data_entrega" placeholder="">
                </div>
                <div class="form-group">
                    <label for="requisicao">Requisição</label>
                    <input type="text" name="requisicao" class="form-control" id="requisicao" placeholder="">
                </div>


                <button type="submit" class="btn btn-primary">Salvar</button>
            </form>
        </div>
    </body>

    </html>
@endsection
