<!DOCTYPE html>
@extends('layout.index')
@section('conteudo')
    <html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>SB Admin 2 - Tables</title>



    </head>


    <body id="page-top">


        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <h1 class="h3 mb-2 text-gray-800">Associações</h1>
            

            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                @if ($liberar_cadastro == true)
                    <div class="card-header py-3">
                        <a href="{{ route('incluir_novo') }}" title="Incluir novo usuario">
                            <button type="button" class="btn btn-primary">Incluir</button>
                        </a>
                    </div>
                @endif


                @if (!empty($msg))
                    <div class="alert alert-success" role="alert">
                        {{ $msg }}
                    </div>
                @endif
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Numero do patrimônio</th>
                                    <th>Nome</th>
                                    <th>Projeto</th>
                                    <th>Filial</th>
                                    <th>Data da Entrega</th>
                                    <th>Tipo</th>
                                    <th>Ação</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Numero do patrimônio</th>
                                    <th>Nome</th>
                                    <th>Projeto</th>
                                    <th>Filial</th>
                                    <th>Data da Entrega</th>
                                    <th>Tipo</th>
                                    <th>Ação</th>
                                </tr>
                            </tfoot>
                            @foreach ($dados as $dados)
                                <tr>
                                    <td>{{ $dados->numero_patrimonio }}</td>
                                    <td>{{ $dados->nome }}</td>
                                    <td>{{ $dados->projeto }}</td>
                                    <td>{{ $dados->filial }}</td>
                                    <td>{{ $dados->data_entrega }}</td>
                                    <td>{{ $dados->tipo }}</td>
                                    <td>
                                        {{-- Form de excluir a associação --}}
                                        <form name='excluir' action="/lotes/excluir/associacao/{{ $dados->id }}"
                                            method="post">
                                            @csrf
                                            <!-- Botão para acionar excluir -->
                                            <div class="col-sm-4">

                                                <button type="button" class="btn btn-link" data-toggle="modal"
                                                    data-target="#p{{ $dados->id }}">
                                                    <i class='fas fa-trash-alt'></i>
                                                </button>
                                            </div>
                                            <!-- Modal Excluir -->
                                            <div class="modal fade" id="p{{ $dados->id }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="{{ $dados->id }}">
                                                                Deseja
                                                                excluir
                                                                o associação?</h5>
                                                            <button type="button" class="close"
                                                                data-dismiss="modal" aria-label="Fechar">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <b>Nome: </b>{{ $dados->nome }}
                                                            <p><b>Foi substituído pelo patrimonio:
                                                                </b>{{ !empty($dados->subistituicao->numero_patrimonio)? $dados->subistituicao->numero_patrimonio: 'Sem substituição' }}
                                                            </p>

                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">Fechar</button>
                                                            @if (empty($dados->subistituicao->numero_patrimonio))
                                                                <button type="subimit"
                                                                    class="btn btn-danger ">EXCLUIR</button>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </form>
                                        {{-- Botão de detalhes --}}
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-link" data-toggle="modal"
                                                data-target="#a{{ $dados->id }}">
                                                <i class="fa-solid fa-magnifying-glass"></i>
                                            </button>
                                        </div>
                                        {{-- Modal de detalhes --}}
                                        <div class="modal fade" id="a{{ $dados->id }}" tabindex="-1" role="dialog"
                                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Detalhes</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p><b>Nome: </b> {{ $dados->nome }}</p>
                                                        <p><b>Numero da requisição: </b>{{ $dados->requisicao }}</p>
                                                        <p><b>Foi substituído pelo patrimonio:
                                                            </b>{{ !empty($dados->subistituicao->numero_patrimonio)? $dados->subistituicao->numero_patrimonio: 'Sem substituição' }}
                                                        </p>
                                                        <p><b>Associador por: </b>{{$dados->usuario->name}}</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Fechar</button>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; Your Website 2020</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->


    </body>

    </html>
@endsection
