@extends('layout.index')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Substituições</title>



</head>
@section('conteudo')

    <body>
        <h1 class="h3 mb-2 text-gray-800">Substituições</h1>
        <!-- DataTales Example -->
        <div class="card shadow mb-4 row">
            <div class="card-header py-3">

            </div>

            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">

                        <thead>
                            <tr>
                                <th>Lote de origem</th>
                                <th>Numero do patrimônio novo</th>
                                <th>Nome</th>
                                <th>Projeto</th>
                                <th>Filial</th>
                                <th>Data da Entrega</th>
                                <th>Numero do patrimônio antigo</th>
                                <th>Tipo</th>
                                <th>Ação</th>

                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Lote de origem</th>
                                <th>Numero do patrimônio novo</th>
                                <th>Nome</th>
                                <th>Projeto</th>
                                <th>Filial</th>
                                <th>Data da Entrega</th>
                                <th>Numero do patrimônio antigo</th>
                                <th>Tipo</th>
                                <th>Ação</th>

                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($dados as $dado)
                                <tr>
                                    <td>{{ $dado->lote->id }}</td>
                                    <td>{{ $dado->numero_patrimonio }}</td>
                                    <td>{{ $dado->nome }}</td>
                                    <td>{{ $dado->projeto }}</td>
                                    <td>{{ $dado->filial }}</td>
                                    <td>{{ $dado->data_entrega }}</td>

                                    <td>{{ !empty($dado->subistituicao->numero_patrimonio)? $dado->subistituicao->numero_patrimonio: 'Sem Subistituição' }}
                                    </td>
                                    <td>{{ $dado->tipo }}</td>

                                    <td>
                                        {{-- Button Subistiuicao --}}
                                        @if (empty($dado->subistituicao->numero_patrimonio))
                                            <button type="button" class="btn btn-link" data-toggle="modal"
                                                data-target="#a{{ $dado->id }}">
                                                <i class="	fa fa-arrows-h"></i>
                                            </button>
                                        @else
                                            {{-- Button Detalhes --}}
                                            <button type="button" class="btn btn-link" data-toggle="modal"
                                                data-target="#b{{ $dado->id }}">
                                                <i class="	fa-solid fa-magnifying-glass"></i>
                                            </button>
                                        @endif
                                    </td>
                                </tr>
                                {{-- Modal Subiastiuicao --}}
                                <div class="modal" id="a{{ $dado->id }}" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Detalhes da Substituição de
                                                    {{ $dado->nome }}</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">

                                                <form method="POST">
                                                    @csrf
                                                    {{-- ID da Associao --}}
                                                    <input type="hidden" name="id_associcao" value={{ $dado->id }}>
                                                    <INPUT TYPE="hidden" name="id_usuario_substituicao"
                                                        value={{ auth()->user()->id }}>
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <label for="numero_patrimonio"><b>Numero do
                                                                    patrimonio:</b></label>
                                                            <input class="form-control form-control" type="text"
                                                                placeholder="" name="numero_patrimonio">
                                                            <small id="numero_patrimonio" class="form-text text-muted">
                                                                Numero de patrimonio do equipamento que está
                                                                sendo
                                                                entregue pelo {{ $dado->nome }}
                                                            </small>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="tipo">
                                                                <b>Tipo Subistituição:</b></label>

                                                            <select class="form-control" id="tipo"
                                                                name="tipo_subistituicao">
                                                                <option value="Roubo">Roubo</option>
                                                                <option value="Mau uso">Mau uso</option>
                                                                <option value="Defeito">Defeito</option>

                                                            </select>

                                                        </div>
                                                        <div class="form-group">
                                                            <label for="motivo">
                                                                <b>Motivo da Subistituição:</b></label>
                                                            <textarea class="form-control" name="motivo" id="exampleFormControlTextarea1" rows="3"></textarea>
                                                        </div>
                                                    </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">Salvar</button>
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Fechar</button>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                {{-- Modal Detalhes --}}

                                <div class="modal fade" id="b{{ $dado->id }}" data-bs-backdrop="static"
                                    data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="staticBackdropLabel">Detalhes da substituição
                                                </h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                @if (!empty($dado->subistituicao->numero_patrimonio))
                                                    <b>Número Patrimonio antigo:</b>
                                                    </b>{{ $dado->subistituicao->numero_patrimonio }}
                                                    <br><b>Motivo: </b> {{ $dado->subistituicao->motivo }}
                                                    <br><b>Tipo: </b>{{ $dado->subistituicao->tipo_subistituicao }}
                                                @endif
                                                <hr size="10" width="100%">
                                                <h5>Detalhes do Lote de Compra </h5>
                                                <br><b>Vendedor:</b> {{ $dado->lote->vendedor }}
                                                <br><b>Modelo: </b> {{ $dado->lote->modelo }}
                                                <br><b>Arquivo Nfe: </b> {{ $dado->lote->nfe }}
                                                <hr size="10" width="100%">
                                                <br><b>Substituição feita pelo: </b> {{ $dado->usuario->name }}
                                            </div>


                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Fechar</button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
@endsection

</html>
