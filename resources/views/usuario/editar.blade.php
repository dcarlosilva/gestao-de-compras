@extends('layout.index')

@section('conteudo')
<h3>Editar: {{$dados->name}}</h3>
    <div class="container">
        <form class="user" method="POST">
            @csrf
            <div class="form-group">
                <label for="nameInput">Nome</label>
                <input type="name" value='{{$dados->name}}' name="name" class="form-control" id="name">
            </div>
            <div class="form-group">
                <label for="emailInput">Email</label>
                <input type="email" name="email" value='{{$dados->email}}' class="form-control" id="email">
            </div>
            <div class="form-group">
                <label for="passwordInput">Senha</label>
                <input type="password" name="password" class="form-control" id="password">
            </div>
            
            <button type="submit" class="btn btn-primary">Salvar</button>
        </form>
    </div>
@endsection
