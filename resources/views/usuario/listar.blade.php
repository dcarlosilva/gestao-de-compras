@extends('layout.index')
@section('conteudo')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Usuários</h1>


        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">
                    <a href="{{ route('usuarios.criar') }}" title="Novo Usuário">
                        <button type="button" class="btn btn-primary">Novo Usuário</button>
                    </a>
                </h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Criado em</th>
                                <th>Ação</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Criado em</th>
                                <th>Ação</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($dados as $dado)
                                <tr>
                                    <td>{{ $dado->name }}</td>
                                    <td>{{ $dado->email }}</td>
                                    <td>{{ $dado->created_at }}</td>
                                    <td>
                                        <div class="col-sm-4">
                                            <a href="{{ route('usuarios.editar', $dado->id) }}" title="Editar Lote ">
                                                <button type="button" class="btn btn-link">
                                                    <i class='far fa-edit'></i>
                                                </button>
                                            </a>
                                        </div>
                                        <form name='excluir' action={{ route('usuarios.excluir', $dado->id) }}
                                            method="post">
                                            @csrf
                                            <div class="col-sm-4">
                                                <!-- Botão para acionar excluir -->
                                                <button type="button" class="btn btn-link" data-toggle="modal"
                                                    data-target="#p{{ $dado->id }}">
                                                    <i class='fas fa-trash-alt'></i>
                                                </button>
                                            </div>
                                            <!-- Modal Excluir -->
                                            <div class="modal fade" id="p{{ $dado->id }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="{{ $dado->id }}">
                                                                Deseja
                                                                excluir
                                                            </h5>
                                                            <button type="button" class="close"
                                                                data-dismiss="modal" aria-label="Fechar">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">

                                                            <p><b>Você selecionou o usuário: </b> {{ $dado->name }}

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">Fechar</button>
                                                            <button type="subimit" class="btn btn-danger ">EXCLUIR</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->
@endsection
