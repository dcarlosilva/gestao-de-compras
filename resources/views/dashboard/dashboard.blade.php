@extends('layout.index')

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
@section('conteudo')

    <body>
        {{-- Quantidades --}}
        <div class="container-fluid">
            <div class="row justify-content-start">
                <!-- Notebook Card -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card  border-left-danger shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        Quantidade disponível de Notebook</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $quantidadeNotebookDisponivel }}
                                        /
                                        {{ $quantidadeNotebook }}</div>
                                </div>
                                <div class="col-auto">

                                    <i class="fas fa-regular fa-laptop fa-2x "></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Celular -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        Quantidade disponível de Celular</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $quantidadeCelularDisponivel }}
                                        /
                                        {{ $quantidadeCelular }} </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fa fa-mobile fa-2x"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Valores --}}
            <div class="row justify-content-start">
                <!-- Notebook Card -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-danger shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        Valor de Compras Notebook</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">
                                        <div id='a'>R$</div>
                                        <script>
                                            formatReal(
                                                getMoney({{ $valorTotalDeComprasNotebook }}), 'a')
                                        </script>
                                    </div>
                                </div>
                                <div class="col-auto">

                                    <i class="fas fa-regular fa-laptop fa-2x "></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Celular -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        Valor de Compras Celular</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">
                                        <div id='b'>R$</div>
                                        <script>
                                            formatReal(
                                                getMoney({{ $valorTotalDeComprasCelular }}), 'b')
                                        </script>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fa fa-mobile fa-2x"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </body>
@endsection

</html>
